/**
 * 
 */
var host="http://localhost";
var port="8080";

function fetchBaseUrl(){
	return host+":"+port;
}

function ajaxUnauthenticatedRequest(httpMethod,relativeUrl,data
		,responseFunction,errorResponseFunction){
	var url="";
	var baseUrl=fetchBaseUrl();
	url=baseUrl+"/joat/webapi";
	url=url+relativeUrl;
	if(httpMethod=="GET" || httpMethod=="DELETE")
		{
			$.ajax({
				url:url,
				headers:{
					'Content-type':'application/json'
				},	
				type:httpMethod,
				success:function(responseData,textStatus,request)
				{
					responseFunction(responseData,textStatus,request);
				},
				error:function(responseData,textStatus,request)
				{
					errorResponseFunction(responseData,textStatus,request);
				}
			});
		}else if (httpMethod == "POST" || httpMethod == "PUT") {
			var formData = "";
			if (data != null) {
				formData = data;
			}
			$.ajax({
				url : url,
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				},
				type : httpMethod,
				data : formData,
				success : function(responseData, textStatus, request) {
					responseFunction(responseData, textStatus, request);
				},
				error : function(responseData, textStatus, request) {
					errorResponseFunction(responseData, textStatus, request);
				}
			});
		}
}