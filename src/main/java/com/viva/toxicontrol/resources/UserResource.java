package com.viva.toxicontrol.resources;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.stereotype.Controller;

import com.viva.toxicontrol.controller.UserController;
import com.viva.toxicontrol.utility.ErrorMessage;
import com.viva.toxicontrol.utility.SecureUtil;
import com.viva.toxicontrol.utility.User;
import com.viva.toxicontrol.utility.Util;
@Path("user")
public class UserResource {
	
	@POST
	@Path("/login")
	@Produces(MediaType.APPLICATION_JSON)
	public Response authenticateUser(@FormParam("userid") String userid,
			@FormParam("password") String password){
		UserController controller=new UserController();
		Util util=new Util();
		User user=controller.authenticateUser(userid,password);
		if(user==null){
		return util.generateErrorResponse(Status.NOT_FOUND, "invalid username or password").build();
		}else
		{
		String session=util.randomStringGenerator(8);
		String token=SecureUtil.issueToken(user, "3QAy*bZn7jW%==LDKK$U", session);
		return Response.status(Status.ACCEPTED).header("X-Authorization", "Bearer"+token).entity(user).build();
	}
	}
	
	@POST
	@Path("/RegisterCompany")
	@Produces(MediaType.APPLICATION_JSON)
	public Response registerUser(@FormParam("company_name") String company_name,
			@FormParam("plot_no") String plot_no,
			@FormParam("contact_person") String contact_person,
			@FormParam("design") String design,
			@FormParam("cont_no") String cont_no,
			@FormParam("email") String email,
			@FormParam("userid") String userid,
			@FormParam("password") String password){
		try {
			User user=new User();
			user.setCompany_name(company_name);
			user.setPlot_no(plot_no);
			user.setContact_person(contact_person);
			user.setDesign(design);
			user.setCont_no(cont_no);
			user.setEmail(email);
			user.setUserid(userid);
			user.setPassword(password);
			UserController controller=new UserController();
			boolean value=controller.searchUserId(userid);
			if(value)
			{
				return Response.status(Status.BAD_REQUEST)
						.entity(new ErrorMessage(Status.BAD_REQUEST.getStatusCode(),
								"username already taken please try something different")).build();
			}
			controller.registerUser(user);
			return Response.status(Status.ACCEPTED).build();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}
		return Util.generateErrorResponse(Status.NOT_ACCEPTABLE, "Data not Accepted.please try again.").build();
		
	}
	
	@GET
	@Path("/search")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchUserId(@QueryParam("searchKey") String key)
	{
		UserController controller=new UserController();
		
		boolean value=controller.searchUserId(key);
		if(value)
		{
			return Response.status(Status.BAD_REQUEST)
					.entity(new ErrorMessage(Status.BAD_REQUEST.getStatusCode(),
							"username already taken please try something different")).build();
		}
		return Response.status(Status.ACCEPTED).build();
		
	}
}