package com.viva.toxicontrol.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.viva.toxicontrol.utility.User;
import com.viva.toxicontrol.utility.Util;

public class UserDao {
	
	public User authenticateUser(String username, String password) {
	Connection con=null;
	PreparedStatement st=null;
	ResultSet rs=null;
	User user=null;
	try {
		con=Util.getDBConnection();
		String query="select * from company_reg where userid=? and password=password(?)";
		st=con.prepareStatement(query);
		st.setString(1, username);
		st.setString(2, password);
		rs=st.executeQuery();
		while(rs.next())
		{
			user=new User();
			user.setId(rs.getLong(1));
			user.setCompany_name(rs.getString(2));
			user.setPlot_no(rs.getString(3));
			user.setContact_person(rs.getString(4));
			user.setDesign(rs.getString(5));
			user.setCont_no(rs.getString(6));
			user.setEmail(rs.getString(7));
			user.setUserid(rs.getString(8));
			user.setPassword("");
			user.setRole(rs.getString(10));
		}
		
	} catch (Exception e) {
		System.out.println(e);
		e.printStackTrace();
	}
	finally {
		Util.closeConnection(rs, st, con);
	}
		return user;
		
	}

	public User registerUser(User user) {
		Connection con=null;
		PreparedStatement st=null;
		try {
			con=Util.getDBConnection();
			String query="insert into `company_reg`(`company`,`plot_no`,`cont_person`,`design`,`cont_no`,"
					+ "`email`,`userid`,`password`,`role`)values(?,?,?,?,?,?,?,password(?),'company')";
			st=con.prepareStatement(query);
			st.setString(1, user.getCompany_name());
			st.setString(2, user.getPlot_no());
			st.setString(3, user.getContact_person());
			st.setString(4, user.getDesign());
			st.setString(5, user.getCont_no());
			st.setString(6, user.getEmail());
			st.setString(7, user.getUserid());
			st.setString(8, user.getPassword());
			st.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}
		finally {
			Util.closeConnection(null, st, con);
		}
		return user;
	}

	public boolean searchUserId(String key) {
		Connection con=null;
		PreparedStatement st=null;
		ResultSet rs=null;
		try {
			con=Util.getDBConnection();
			String query="select userId from company_reg where userid=?";
			st=con.prepareStatement(query);
			st.setString(1, key);
			rs=st.executeQuery();
			while(rs.next())
			{
				return true;
			}
			return false;
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}
		finally {
			Util.closeConnection(rs, st, con);
		}
		return true;
	}
}
