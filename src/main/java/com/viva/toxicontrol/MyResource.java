package com.viva.toxicontrol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyResource {

	public static void main(String[] args) {
		SpringApplication.run(MyResource.class,args);
	}
}
