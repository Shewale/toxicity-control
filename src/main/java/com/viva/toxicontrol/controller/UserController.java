package com.viva.toxicontrol.controller;

import org.springframework.beans.factory.annotation.Autowired;

import com.viva.toxicontrol.dao.UserDao;
import com.viva.toxicontrol.utility.User;

public class UserController {

	
	public User authenticateUser(String username, String password) {
		UserDao dao=new UserDao();
		return dao.authenticateUser(username,password);
		
	}

	public User registerUser(User user) {
		UserDao dao=new UserDao();
		return dao.registerUser(user);	
	}

	public boolean searchUserId(String key) {
		UserDao dao=new UserDao();
		return dao.searchUserId(key);
	}
}
