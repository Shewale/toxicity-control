package com.viva.toxicontrol.utility;

import javax.ws.rs.FormParam;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class User {
	private long id;
	
	private String company_name;

	private String plot_no;
	
	private String contact_person;
	
	private String design;
	
	private String cont_no;
	
	private String email;
	
	private String userid;
	
	private String password;
	
	private String role;
	
	public User() {

	}
	
	
	public User(long id, String company_name, String plot_no, String contact_person, String design,
			String cont_no, String email,String userid,String password,String role) {
		super();
		this.id = id;
		this.company_name = company_name;
		this.plot_no = plot_no;
		this.contact_person = contact_person;
		this.design = design;
		this.cont_no = cont_no;
		this.email = email;
		this.userid = userid;
		this.password = password;
		this.role = role;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getCompany_name() {
		return company_name;
	}


	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}


	public String getPlot_no() {
		return plot_no;
	}


	public void setPlot_no(String plot_no) {
		this.plot_no = plot_no;
	}


	public String getContact_person() {
		return contact_person;
	}


	public void setContact_person(String contact_person) {
		this.contact_person = contact_person;
	}


	public String getDesign() {
		return design;
	}


	public void setDesign(String design) {
		this.design = design;
	}


	public String getCont_no() {
		return cont_no;
	}


	public void setCont_no(String cont_no) {
		this.cont_no = cont_no;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getUserid() {
		return userid;
	}


	public void setUserid(String userid) {
		this.userid = userid;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getRole() {
		return role;
	}


	public void setRole(String role) {
		this.role = role;
	}
	
	

	
	
}
