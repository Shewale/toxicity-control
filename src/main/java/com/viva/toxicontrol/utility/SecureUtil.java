package com.viva.toxicontrol.utility;

import java.util.Calendar;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
public class SecureUtil {
	public static String issueToken(User user,String signingkey,String session)
	{
		String jwt="";
		try {
			Calendar today=Calendar.getInstance();
			Calendar tommorow=Calendar.getInstance();
			
			tommorow.add(Calendar.DATE, 1);
			jwt=Jwts.builder().setSubject(user.getUserid()).claim("company",user.getCompany_name())
					.claim("plot_no",user.getPlot_no()).claim("cont_person", user.getContact_person())
					.claim("cont_no", user.getCont_no()).claim("design", user.getDesign()).claim("email", user.getEmail())
					.claim("role", user.getRole()).claim("session", session).setIssuedAt(today.getTime())
					.setExpiration(tommorow.getTime()).signWith(SignatureAlgorithm.HS256, signingkey).compact();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return jwt;
		
	}
}
